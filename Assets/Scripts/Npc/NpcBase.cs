using startup.single;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcBase : MonoBehaviour
{

    private static NpcHUD _NpcHUDPrefab;

    private static NpcDataScriptableObject _NpcDataScriptableObject;

    [Header("# NPC 정보")]
    public string m_NpcCode;

    /// <summary>
    /// HUD 가 표시될 위치를 나타냅니다.
    /// </summary>
    public Transform m_NpcHUDWorldPosition;

    /// <summary>
    /// 상호작용 가능 반경
    /// </summary>
    [Header("# 상호작용")]
    public float m_InteractRadius = 3.0f;

    /// <summary>
    /// 상호작용 시 뷰 타깃으로 지정할 트랜스폼
    /// </summary>
    public Transform m_ViewTargetTransform;

    private NpcData _NpcData;

    public ref NpcData npcData => ref _NpcData;

    /// <summary>
    /// NPC HUD 객체입니다.
    /// </summary>
    public NpcHUD npcHUD { get; private set; }

    protected virtual void Awake()
    {

        if(_NpcHUDPrefab == null)
        {
            _NpcHUDPrefab = Resources.Load<NpcHUD>("Prefabs/UI/HUD/HUD_NPC");
        }
        // NPC 데이터 초기화
        InitializeNpcData();

        // NPC HUD 생성
        CreateNpcHUD();
    }
    protected virtual void Start()
    {
    }

    protected virtual void Update()
    {
        UpdateHUDPosition();
    }

    private void CreateNpcHUD()
    {
        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        npcHUD = sceneInstance.m_UsePlayerUI.CreateHUD(_NpcHUDPrefab);
        npcHUD.InitializeNpcHUD(npcData.npcName);
    }

    private void UpdateHUDPosition()
    {
        npcHUD.SetWorldPosition(m_NpcHUDWorldPosition.position);
    }

    private ref NpcData InitializeNpcData()
    {
        if (_NpcDataScriptableObject == null)
        {
            _NpcDataScriptableObject = Resources.Load<NpcDataScriptableObject>("ScriptableObject/NpcData");
        }

        _NpcData = _NpcDataScriptableObject.GetNpcData(m_NpcCode);

        return ref _NpcData;
    }

    /// <summary>
    /// 상호작용이 가능해질 때 한 번 호출됩니다.
    /// </summary>
    public virtual void OnInteractableStarted()
    {
    }

    /// <summary>
    /// 상초작용이 불가능해질 때 한번 호출됩니다.
    /// </summary>
    public virtual void OnInteractableFinished()
    {
    }

    /// <summary>
    /// 상호작용 시 호출되는 메서드입니다.
    /// </summary>
    public virtual void OnInteraction() 
    {
        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        // NPC UI 생성
        NpcUI npcUI = sceneInstance.m_UsePlayerUI.CreateNpcUI(in npcData);

        // UI 입력 모드로 변경
        sceneInstance.playerController.SetControlMode(Constants.ACTIONMAP_UI_MODE);

        // 뷰 타깃 설정
        SpringComponent springComponent = sceneInstance.m_UseCamera.GetComponentInParent<SpringComponent>();

        springComponent.SetViewTarget(m_ViewTargetTransform);

        // NPC UI 가 닫힐 때 뷰 타깃 취소
        npcUI.onNpcUIClosed += springComponent.CancelViewTarget;
    }
}
