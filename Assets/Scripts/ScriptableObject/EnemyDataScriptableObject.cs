using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "ScriptableObject/EnemyData")]
public class EnemyDataScriptableObject : ScriptableObject
{
    /// <summary>
    /// 적 정보들을 나타냅니다.
    /// </summary>
    public List<EnemyDataElem> m_EnemyDatas;

    public EnemyData GetEnemyData(string enemyCode)
    {
        EnemyDataElem findElem = m_EnemyDatas.Find((elem) => elem.m_EnemyCode == enemyCode);
        return findElem.m_EnemyData;
    }
}