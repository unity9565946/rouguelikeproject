using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NpcData", menuName = "ScriptableObject/NpcData")]
public class NpcDataScriptableObject : ScriptableObject
{
    public List<NpcData> m_NpcDatas;

    public NpcData GetNpcData(string npcCode)
        => m_NpcDatas.Find((npcData) => npcData.npcCode == npcCode);
}