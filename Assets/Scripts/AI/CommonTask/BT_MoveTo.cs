using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BT_MoveTo : RunnableBehavior
{
    /// <summary>
    /// 목표 위치를 담는 키 이름을 나타냅니다.
    /// </summary>
    private string _TargetPositionKeyName;

    /// <summary>
    /// 이동을 멈출때까지 대기합니다.
    /// </summary>
    private WaitUntil _WaitMoveFinished;

    public BT_MoveTo(string targetPositionKeyName)
    {
        _TargetPositionKeyName = targetPositionKeyName;

        // 행동 끝 조건을 설정합니다.
        _WaitMoveFinished = new(() =>
        GetBehaviorController<BehaviorController_Enemy>().navMeshAgent.isStopped);
    }

    public override IEnumerator RunBehavior()
    {
        // Get NavMeshAgent Component
        NavMeshAgent navMeshAgent = GetBehaviorController<BehaviorController_Enemy>().navMeshAgent;

        // 이동 목표 위치를 얻습니다.
        Vector3 targetPosition = behaviorController.GetKeyAsValue<Vector3>(_TargetPositionKeyName);

        // 목표 위치 설정
        navMeshAgent.SetDestination(targetPosition);

        // 목표 위치에 도달할 때까지 대기합니다.
        while(true)
        {
            yield return _WaitMoveFinished;

            isSucceeded = true;
            yield break;
        }
    }

    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        NavMeshAgent agent = behaviorController.GetComponent<NavMeshAgent>();

        // 그릴 원의 반지름
        float circleRadius = agent.radius;

        Vector3 offset = Vector3.up * 0.2f;

        // 현재 위치
        Vector3 currentPosition = behaviorController.transform.position;

        // 목표 위치
        Vector3 targetPosition = agent.destination;

        UnityEditor.Handles.color = Color.black;
        UnityEditor.Handles.DrawWireDisc(currentPosition + offset, Vector3.up, circleRadius);
        UnityEditor.Handles.DrawWireDisc(targetPosition + offset, Vector3.up, circleRadius);
        UnityEditor.Handles.DrawLine(currentPosition, targetPosition);
    }
}
