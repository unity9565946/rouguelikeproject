using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class BT_GetRandomPositionInNavigableRadius : RunnableBehavior
{
    public override IEnumerator RunBehavior()
    {
        for (int i = 0; i < 50; ++i)
        {
            // 최대 이동 거리를 얻습니다.
            float maxMoveDistance = behaviorController.GetKeyAsValue<float>(
                BehaviorController_Enemy.KEYNAME_MAXMOVEDISTANCE);

            Vector3 spawnPosition = behaviorController.GetKeyAsValue<Vector3>(
                BehaviorController_Enemy.KEYNAME_SPAWNPOSITION);

            // 생성 지점부터 랜덤하게 이동시킬 위치까지의 거리를 설정합니다.
            float randomDistance = Random.Range(0.0f, maxMoveDistance);

            // 랜덤한 방향을 얻습니다.
            Vector3 randomDirection = Random.insideUnitSphere;

            // 이동할 위치를 계산합니다.
            Vector3 newTargetPosition = spawnPosition + (randomDirection * randomDistance);

            // 이동 가능한 위치인지 확인합니다.
            if (NavMesh.SamplePosition(
                newTargetPosition,
                out NavMeshHit hit,
                maxMoveDistance,
                NavMesh.AllAreas))
            {
                // 목표 위치를 설정합니다.
                behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_MOVETARGETPOSITION, newTargetPosition);

                // 행동 실행 결과를 성공으로 설정합니다.
                isSucceeded = true;
                yield break;
            }
        }
        isSucceeded = false;
    }

}