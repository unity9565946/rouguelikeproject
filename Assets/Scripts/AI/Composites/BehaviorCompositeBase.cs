using System.Collections.Generic;

public abstract class BehaviorCompositeBase : RunnableBehavior
{
    // 순차적으로 실행시킬 행동 객체들을 나타냅니다.
    protected List<System.Func<RunnableBehavior>> m_Runnable = new();

    public BehaviorCompositeBase() { }

    public BehaviorCompositeBase(params System.Func<RunnableBehavior>[] runnables)
    {
        foreach (System.Func<RunnableBehavior> runnable in runnables)
        {
            AddBehavior(runnable);
        }
    }

    /// <summary>
    /// 행동을 추가합니다.
    /// 기본 생성자를 사용하는 경우 사용할 수 있는 메서드입니다.
    /// </summary>
    /// <typeparam name="TRunnableBehavior"></typeparam>
    public void AddBehavior<TRunnableBehavior>()
        where TRunnableBehavior : RunnableBehavior, new() => m_Runnable.Add(Create<TRunnableBehavior>());

    /// <summary>
    /// 행동을 추가합니다.
    /// 기본 생성자 외에도 사용 가능한 메서드입니다.
    /// </summary>
    /// <param name="runnable"></param>
    public void AddBehavior(System.Func<RunnableBehavior> runnable) => m_Runnable.Add(runnable);
}