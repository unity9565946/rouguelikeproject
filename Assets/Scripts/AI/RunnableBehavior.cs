using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BehaviorController 를 통해 실행될 수 있는 객체의 최상위 기반 형식입니다.
/// </summary>
public abstract class RunnableBehavior
{
    /// <summary>
    /// 이 객체에서 사용되는 서비스 객체들을 나타냅니다.
    /// </summary>
    private List<System.Func<BehaviorSerivce>> _Services = new();

    /// <summary>
    /// 서비스 루틴을 나타냅니다.
    /// </summary>
    private Coroutine _ServiceRoutine;

    /// <summary>
    /// 생성된 서비스 객체들을 나타냅니다.
    /// </summary>
    public List<BehaviorSerivce> behaviorSerivces { get; private set; }

    /// <summary>
    /// 이 행동을 실행하는 BehaviorController 객체입니다.
    /// </summary>
    public BehaviorController behaviorController { get ; private set; }  

    /// <summary>
    /// 실행시킨 행동의 성공 여부를 나타내는 프로퍼티입니다.
    /// </summary>
    public bool isSucceeded { get; protected set; }

    /// <summary>
    /// 행동 객체를 등록하기 위해 사용되는 함수입니다.
    /// </summary>
    /// <typeparam name="TRunnableBehavior">등록시킬 Task 나 Composite 형태를 전달합니다.</typeparam>
    /// <returns>행동 객체를 생성하기 위한 구문을 반환합니다.</returns>
    protected static System.Func<RunnableBehavior> Create<TRunnableBehavior>()
        where TRunnableBehavior : RunnableBehavior, new() =>/*return*/() => new TRunnableBehavior();


    /// <summary>
    /// 객체 초기화
    /// 생성자 호출 이후에 호출됩니다,
    /// </summary>
    public virtual void OnRunnableInitialized(BehaviorController behaviorController)
    {
        this.behaviorController = behaviorController;

        // 실행시킬 서비스가 존재하는 경우
        if(_Services.Count != 0)
        {
            behaviorSerivces = new();
            foreach(System.Func<BehaviorSerivce> getService in _Services) 
            {
                // 서비스 객체를 생성합니다.
                BehaviorSerivce runService = getService.Invoke();

                // 서비스 객체를 초기화합니다.
                runService.OnServiceStarted(behaviorController);

                // 실행시킬 서비스 객체 등록
                behaviorSerivces.Add(runService);
            }

            // 서비스 실행
            _ServiceRoutine = behaviorController.StartCoroutine(RunService());
        }
    }

    /// <summary>
    /// 행동을 실행시킵니다.
    /// 구현 시 내부에서 행동 실행 성공 여부를 isSucceeded에 설정해야 합니다.
    /// </summary>
    /// <returns></returns>
    public abstract IEnumerator RunBehavior();

    /// <summary>
    /// 서비스 틱을 위한 루틴입니다.
    /// </summary>
    /// <returns></returns>
    public virtual IEnumerator RunService()
    {
        while (true) 
        {
            foreach (BehaviorSerivce serivce in behaviorSerivces)
                serivce.ServiceTick();
            yield return null;
        }
    }

    /// <summary>
    /// 행동이 끝났을 때 호출됩니다.
    /// </summary>
    public virtual void OnBehaviorFinished() 
    {
        // 서비스를 실행하고 있는 경우
        if(_ServiceRoutine != null)
        {
            // 서비스 루틴 종료
            behaviorController.StopCoroutine(_ServiceRoutine);
            _ServiceRoutine = null;

            // 서비스 종료
            foreach (BehaviorSerivce serivce in behaviorSerivces)
                serivce.OnServiceFinished();

            behaviorSerivces.Clear();
        }
    }

    public TBehaviorController GetBehaviorController<TBehaviorController>()
        where TBehaviorController : BehaviorController
        => behaviorController as TBehaviorController;

    /// <summary>
    /// 사용할 서비스를 추가합니다.
    /// </summary>
    /// <typeparam name="TBehaviorService">서비스 형식을 전달합니다.</typeparam>
    public void AddService<TBehaviorService>()
        where TBehaviorService : BehaviorSerivce, new()
        => _Services.Add( () => new TBehaviorService() );
    public void AddService(System.Func<BehaviorSerivce> runService)
        => _Services.Add(runService);

    public virtual void OnDrawGizmosSelected() { }
    public virtual void OnDrawGizmos() { }
}
