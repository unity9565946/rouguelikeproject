using System.Collections;
using UnityEngine;

public class BT_Attack_Horseman : RunnableBehavior
{
    public override IEnumerator RunBehavior()
    {
        // 공격 컴포넌트를 얻습니다.
        HorsemanAttack attackComponent = behaviorController.GetComponent<HorsemanAttack>();

        // 공격 요청이 들어왔으므로 요청 처리
        behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_ATTACKREQUESTED, false);

        // 공격 상태로 전환
        behaviorController.SetKey(BehaviorController_Horseman.KEYNAME_ISATTACKING, true);

        // 공격 실행
        attackComponent.Attack();

        // 공격이 끝날 때까지 대기
        yield return new WaitWhile(() => behaviorController.GetKeyAsValue<bool>(BehaviorController_Horseman.KEYNAME_ISATTACKING));

        // 행동 성공
        isSucceeded = true;
        yield return null;

    }
}