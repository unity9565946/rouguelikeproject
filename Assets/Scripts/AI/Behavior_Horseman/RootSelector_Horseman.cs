using UnityEngine;
using System.Collections;

public class RootSelector_Horseman : BehaviorSelector
{
    public RootSelector_Horseman()
    {
        // 대기 상태 시퀀서
        AddBehavior<WaitDamageSqeuncer>();

        // 공격 상태 시퀀서
        AddBehavior<AggressiveSelector>();
    }

    /// <summary>
    /// 대기 상태일 때 실행시킬 시퀀서 입니다.
    /// </summary>
    public class WaitDamageSqeuncer : BehaviorSequencer
    {
        public WaitDamageSqeuncer() 
        {
            // 대기
            AddBehavior(() => new BT_Wait(1.0f));


            // 랜덤한 위치 뽑기
            AddBehavior<BT_GetRandomPositionInNavigableRadius>();

            // 랜덤한 위치로 이동
            AddBehavior(() => new BT_MoveTo(BehaviorController_Horseman.KEYNAME_MOVETARGETPOSITION));
        }

        public override IEnumerator RunBehavior()
        {
            // 공격적인 상태인지 확인합니다.
            bool isAggressiveState = behaviorController.GetKeyAsValue<bool>(BehaviorController_Enemy.KEYNAME_ISAGGRESSIVESTATE);

            // 공격적인 상태인 경우 행동 실패!
            if(isAggressiveState) 
            {
                isSucceeded = false;
                yield break;
            }

            // 기존 로직 실행
            yield return base.RunBehavior();
        }

    }
    public class AggressiveSelector : BehaviorSelector
    {
        public AggressiveSelector() 
        {
            // 플레이어 캐릭터 추적 상태
            AddBehavior<TrackingPlayerSequencer>();

            // 공격 행동
            AddBehavior<BT_Attack_Horseman>();
        }

    }

    public class TrackingPlayerSequencer : BehaviorSequencer
    {
        public TrackingPlayerSequencer()
        {
            // 캐릭터 감지 서비스 추가
            AddService(() => new BS_DoDetectPlayer(
                2.0f,
                LayerMask.GetMask("PlayerCharacter")));

            // 플레이어 위치 얻기 행동 추가
            AddBehavior(() => new BT_GetPlayerPosition(
                BehaviorController_Enemy.KEYNAME_MOVETARGETPOSITION, 
                BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER,
                2.0f));
            
            // 목표 위치로 이동 행동 추가
            AddBehavior(() => new BT_MoveTo(BehaviorController_Enemy.KEYNAME_MOVETARGETPOSITION));
        }

        public override IEnumerator RunBehavior()
        {
            // 공격 요청 여부를 확인합니다.
            bool attackRequested = behaviorController.GetKeyAsValue<bool>(
                BehaviorController_Enemy.KEYNAME_ATTACKREQUESTED);

            // 공격 요청을 반은 경우 행동을 종료하도록 합니다.
            if(attackRequested)
            {
                // 행동 실패
                isSucceeded = false;

                // 행동 즉시 종료
                yield break;
            }

            // 기존 로직을 실행
            yield return base.RunBehavior();
        }
    }
}