using UnityEngine;
using UnityEngine.AI;

public sealed class BehaviorController_Dummy : BehaviorController_Enemy
{
    protected override void Awake()
    {
        base.Awake();

        // 최대 이동 가능 거리를 설정합니다.
        SetKey(KEYNAME_MAXMOVEDISTANCE, 15.0f);

    }

    protected override void Start()
    {
        base.Start();

        StartBehavior<RootSequencer_Dummy>();
    }
}