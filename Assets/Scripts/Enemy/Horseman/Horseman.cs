using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horseman : EnemyCharacter
{

    /// <summary>
    /// 애니메이션 파라미터 _MoveSpeed 에 적용시킬 수치
    /// </summary>
    private float _AnimAPramMoveSpeedValue;

    private HorsemanAttack _AttackComponent;

    /// <summary>
    /// 랙돌을 위한 Rigidbody Component 들
    /// </summary>
    private List<Rigidbody> _SpineRigidBodies;

    public HorsemanAttack attackComponent => _AttackComponent ?? (_AttackComponent = GetComponent<HorsemanAttack>());

    protected override void Awake()
    {
        base.Awake();

        _SpineRigidBodies = new(GetComponentsInChildren<Rigidbody>());
        foreach (Rigidbody rigidbody in _SpineRigidBodies) 
        {
            rigidbody.useGravity = false;
            rigidbody.isKinematic = true;
            rigidbody.gameObject.layer = LayerMask.NameToLayer(Constants.LAYERNAME_RAGDOLL);
        }

    }

    protected override void Update()
    {
        base.Update();

        UpdateAnimParam();
    }

    private void UpdateAnimParam()
    {
        _AnimAPramMoveSpeedValue = Mathf.MoveTowards(_AnimAPramMoveSpeedValue, agent.velocity.magnitude, 10f * Time.deltaTime);
        

        // 속력값 갱신
        animController.SetFloat(Constants.ANIMPARAM_HORSEMAN_MOVESPEED,
            _AnimAPramMoveSpeedValue);
    }

    public override void OnDamaged(PlayerCharacter playerCharacter, float damage)
    {
        base.OnDamaged(playerCharacter, damage);

        HorsemanAnimController animController = (this.animController as  HorsemanAnimController);

        // 공격 영역 비활성화
        attackComponent.DisAbleAttackArea();

        // 공격 끝내기
        animController.AnimEvent_AttackFinished();

        // Hit 애니메이션 재생
        animController.PlayHitAnimation();

        // 피해를 입을 경우 행동을 재실행합니다.
        behaviorController.StopBehavior();
        (behaviorController as BehaviorController_Horseman).StartBehavior();
    }

    public override void OnDead()
    {
        base.OnDead();



        // 애니메이터 중단
        animController.animator.enabled = false;

        // 행동 정지
        behaviorController.StopBehavior();

        foreach (Rigidbody rigidbody in _SpineRigidBodies)
        {
            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;
            rigidbody.velocity = Vector3.zero;
        }

        Destroy(gameObject, 5.0f);
    }

}
