using startup.single;
using UnityEngine;

public class HUDBase : MonoBehaviour
{
    /// <summary>
    /// 표시될 월드 위치를 나타냅니다.
    /// </summary>
    private Vector3 _WorldPosition;

    /// <summary>
    /// 사용되는 카메라 객체를 나타냅니다.
    /// </summary>
    private Camera _Camera;

    private RectTransform _RectTransform;

    public Vector3 worldPosition => _WorldPosition;

    public RectTransform rectTransform => _RectTransform ?? (_RectTransform = transform as RectTransform);

    /// <summary>
    /// 해당 HUD 가 표시되고 있음을 나타냅니다.
    /// </summary>
    public bool isShowing { get; private set; } = true;

    protected virtual void Awake()
    {
        // Get SceneInstance
        GameSceneInstance sceneInst = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        // Get Camera Component
        _Camera = sceneInst.m_UseCamera;

        rectTransform.sizeDelta = rectTransform.anchorMin = rectTransform.anchorMax = rectTransform.pivot = Vector2.zero;

    }

    protected virtual void Update()
    {
        UpdateScreenPosition();
    }

    private void UpdateScreenPosition()
    {
        // 화면에 표시될 HUD의 위치를 계산합니다.
        Vector3 ScreenPosition = _Camera.WorldToViewportPoint(_WorldPosition);

        ScreenPosition.x *= (Screen.width / PlayerUI.screenRatio);
        ScreenPosition.y *= (Screen.height / PlayerUI.screenRatio);

        rectTransform.anchoredPosition = ScreenPosition;

        if(ScreenPosition.z < 0.0f)
        {
            if(isShowing) 
            {
                isShowing = false;
                OnHUDHide();
            }
        }
        else
        {
            if(!isShowing) 
            {
                isShowing = true;
                OnHUDShow();
            }
        }
    }

    /// <summary>
    /// HUD 가 표시될 월드 위치를 설정합니다.
    /// </summary>
    /// <param name="newWorldPosition">설정시킬 월드 위치를 전달합니다.</param>
    public void SetWorldPosition(Vector3 newWorldPosition)
    {
        _WorldPosition = newWorldPosition;
    }

    /// <summary>
    /// HUD 가 숨겨질 때 호출됩니다.
    /// Z 축 값이 음수가 되는 경우 호출됩니다.
    /// </summary>
    protected virtual void OnHUDHide()
    {

    }

    /// <summary>
    /// HUD 가 표시될 때 호출됩니다.
    /// Z 축 값이 양수가 되는 경우 호출됩니다.
    /// </summary>
    protected virtual void OnHUDShow() 
    {

    }
}