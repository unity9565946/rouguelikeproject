using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHUD : HUDBase
{
    [Header("# 이름")]
    public TMP_Text m_NameText;

    [Header("# 체력")]
    public Image m_HpbarImage;

    List<Graphic> m_HUDGraphics;

    /// <summary>
    /// 최대 체력 수치를 나타냅니다.
    /// </summary>
    private float _MaxHP;

    /// <summary>
    /// 적 HUD 를 초기화합니다.
    /// </summary>
    /// <param name="enemyName">적 이름을 전달합니다.</param>
    /// <param name="maxHP">적 최대체력을 전달합니다.</param>
    public void InitializeEnemyHUD(string enemyName, float maxHP)
    {
        m_NameText.text = enemyName;
        m_HpbarImage.fillAmount = 1.0f;
        _MaxHP = maxHP;

        // Graphic 의 하위 클래스 캑체들을 모두 얻습니다.
        m_HUDGraphics = new();
        foreach (Graphic graphic in GetComponentsInChildren<Graphic>()) 
        {
            m_HUDGraphics.Add(graphic);
        }
    }

    public void SetHp(float newHp)
    {
        m_HpbarImage.fillAmount = newHp / _MaxHP;
    }

    protected override void OnHUDHide()
    {
        base.OnHUDHide();

        foreach (Graphic graphic in m_HUDGraphics)
        {
            Color currentColor = graphic.color;
            currentColor.a = 0.0f;
            graphic.color = currentColor;
        }
    }

    protected override void OnHUDShow()
    {
        base.OnHUDShow();

        foreach (Graphic graphic in m_HUDGraphics)
        {
            Color currentColor = graphic.color;
            currentColor.a = 1.0f;
            graphic.color = currentColor;
        }
    }
}
