using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackResultHUD : HUDBase
{
    private AttackResultHUDController _AttackResultHUDController;

    public void InitializeAttackResultHUD(AttackResultHUDController attackResultHUDController)
    {
        _AttackResultHUDController = attackResultHUDController;

        // 제거 타이머 실행
        StartCoroutine(DestroyTimer());
    }

    protected override void Update()
    {
        base.Update();

        // 조금씩 위로 이동되도록 합니다.
        Vector3 currentPosition = worldPosition;
        currentPosition.y += 1 * Time.deltaTime;
        SetWorldPosition(currentPosition);
          
    }

    private IEnumerator DestroyTimer()
    {
        yield return new WaitForSeconds(Constants.ATTACKRESULT_LIFETIMESECONDS);

        _AttackResultHUDController.DestroyAttackResultHUD(this);
    }
}
