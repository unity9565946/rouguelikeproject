using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStaminaUI : MonoBehaviour
{
    [Header("# Stamina UI")]
    public Image m_StaminaValueImage;
    public TMP_Text m_StaminaValueText;

    private float _MaxStamina;

    public void InitializePlayerStaminaUI(float maxStamina)
    {
        _MaxStamina = maxStamina;

        OnStaminaValueChanged(_MaxStamina);
    }

    public void OnStaminaValueChanged(float newStamina)
    {
        m_StaminaValueImage.fillAmount = newStamina / _MaxStamina;

        // 텍스트 설정
        m_StaminaValueText.text = $"{(int)newStamina} / {(int)_MaxStamina}";

    }

}
