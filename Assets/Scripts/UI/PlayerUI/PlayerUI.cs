using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    private static NpcUI _NpcUIPrefab;

    [Header("# Player State UI")]
    public PlayerHPUI m_PlayerHP;
    public PlayerStaminaUI m_PlayerStamina;

    [Header("# NpcUI Panel")]
    public RectTransform m_NpcUIPanel;


    /// <summary>
    /// HUD 들이 추가될 패널을 나타냅니다.
    /// </summary>
    public RectTransform m_HUDPanel;

    /// <summary>
    /// 화면 비율을 나타냅니다.
    /// </summary>
    public static float screenRatio {  get; private set; }

    protected virtual void Awake()
    {
        if (_NpcUIPrefab == null)
        {
            _NpcUIPrefab = Resources.Load<NpcUI>("Prefabs/UI/NpcUI/UI_Npc");
        }


        screenRatio = transform.localScale.x;
    }

    protected virtual void Update()
    {
        screenRatio = transform.localScale.x;
    }

    /// <summary>
    /// HUD 객체를 등록합니다.
    /// </summary>
    public T CreateHUD<T>(T hudPrefab)
        where T : HUDBase
    {
        // HUD 객체 생성
        T hudInstance = Instantiate(hudPrefab);

        // HUD 패널에 추가
        hudInstance.transform.SetParent(m_HUDPanel, transform);

        // 생성한 HUD 객체 반환
        return hudInstance;
    }

    public NpcUI CreateNpcUI(in NpcData npcData)
    {
        NpcUI npcUI = Instantiate(_NpcUIPrefab, m_NpcUIPanel);
        npcUI.InitializeNpcUI(npcData);

        return npcUI;
    }
}
