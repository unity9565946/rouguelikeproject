using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHPUI : MonoBehaviour
{
    [Header("# HP UI")]
    public Image m_HPValueImage;
    public Image m_HPValueFollowImage;
    public TMP_Text m_HPValueText;

    private float _MaxHP;
    private float _CurrentHP;

    /// <summary>
    /// 체력이 변경될 때 마다 호출되는 메서드입니다.
    /// </summary>
    /// <param name="newHP"></param>
    public void OnHPValueChanged(float newHP)
    {
        _CurrentHP = newHP;

        float fill = _CurrentHP / _MaxHP;

        m_HPValueImage.fillAmount = fill;

        // 텍스트 설정
        m_HPValueText.text = $"{_CurrentHP} / {_MaxHP}";
    }

    public void InitializePlayerHPUI(float maxHP)
    {
        _CurrentHP = _MaxHP = maxHP;

        OnHPValueChanged(_CurrentHP);

        m_HPValueFollowImage.fillAmount = m_HPValueImage.fillAmount;
    }

    private void Update()
    {
        UpdateHPValueFollowFillAmount();
    }

    private void UpdateHPValueFollowFillAmount()
    {
        float targetFill = m_HPValueImage.fillAmount;

        float currentFill = m_HPValueFollowImage.fillAmount;

        m_HPValueFollowImage.fillAmount = Mathf.Lerp(currentFill, targetFill, 0.1f);
    }
}
