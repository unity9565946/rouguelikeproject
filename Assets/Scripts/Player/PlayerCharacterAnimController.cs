using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 플레이어 캐릭터의 애니메이션을 컨트롤 시키기 위한 컴포넌트입니다.
/// </summary>
public class PlayerCharacterAnimController : MonoBehaviour
{
    private Animator _Animator;
    public Animator animator => _Animator ?? (_Animator = GetComponent<Animator>());


    public float moveSpeed { set => animator.SetFloat(Constants.ANIMPARAM_MOVESPEED, value); }
    public bool isGrounded { set => animator.SetBool(Constants.ANIMPARAM_ISGROUNDED, value); }

    public int currentCombo { set => animator.SetInteger(Constants.ANIMPARAM_CURRENTCOMBO, value); }

    public bool isBlocked { set => animator.SetBool(Constants.ANIMPARAM_BLOCKED, value); }

    public bool isAttackFinished { set => animator.SetBool(Constants.ANIMPARAM_ATTACKFINISHED, value); }

    /// <summary>
    /// 이동 입력을 허용시키는 이벤트
    /// </summary>
    public event System.Action onMovementInputAllowed;
    
    /// <summary>
    /// 다음 공격 입력을 확인하기 시작합니다.
    /// </summary>
    public event System.Action onNextAttackInputCheckStarted;

    /// <summary>
    /// 다음 공격 입력 확인을 끝냅니다.
    /// </summary>
    public event System.Action onNextAttackInputCheckFinished;

    /// <summary>
    /// 하나의 공격이 끝났을 경우 발생하는 이벤트
    /// </summary>
    public event System.Action onAttackSequenceFinished;

    /// <summary>
    /// 공격 영역 활성화 / 비활성화를 위한 이벤트
    /// </summary>
    public event System.Action<bool /* bEnable*/> setAttackAreaEnable;


    public void StartEquipWeaponAnimation()
    {
        animator.SetTrigger(Constants.ANIMPARAM_STARTEQUIPWEAPON);
    }

    public void StartUnEquipWeaponAnimation()
    {
        animator.SetTrigger(Constants.ANIMPARAM_STARTUNEQUIPWEAPON);
    }

    public void StartHitAnimation()
    {
        animator.SetTrigger(Constants.ANIMPARAM_ISHIT);
    }


    public void OnAttackAnimationRequest(string attackName)
    {
        animator.Play(attackName);
        
    }
    public void AnimEvent_OnMovementInputAllowed() => onMovementInputAllowed?.Invoke();

    public void AnimEvent_StartNextAttackCheck()
    {
        onNextAttackInputCheckStarted?.Invoke();
    }

    public void AnimEvent_FinishNextAttackCheck()
    {
        onNextAttackInputCheckFinished?.Invoke();
    }

    public void AnimEvent_AttackFinished()
    {
        onAttackSequenceFinished?.Invoke();
    }

    public void AnimEvent_EnableAttackArea()
    {
        setAttackAreaEnable?.Invoke(true);
    }

    public void AnimEvent_DisableAttackArea()
    {
        setAttackAreaEnable?.Invoke(false);
    }
}
