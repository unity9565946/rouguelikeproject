using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    /// <summary>
    /// 상호 작용 가능한 NPC 객체들을 나타냅니다.
    /// </summary>
    private List<NpcBase> _InteractableNpcs = new();

    /// <summary>
    /// 상호작용 가능한 NPC 객체를 추가합니다.
    /// </summary>
    /// <param name="npc"></param>
    public void AddInteractableNpc(NpcBase npc)
    {
        // 이미 추가되어있는 경우 제외
        if(_InteractableNpcs.Contains(npc)) return; 

        npc.OnInteractableStarted();

        _InteractableNpcs.Add(npc);

        // 가까운 순으로 정렬
        _InteractableNpcs.Sort((a, b) =>
        Vector3.Distance(a.transform.position, transform.position) <
        Vector3.Distance(b.transform.position, transform.position) ? -1 : 1);
    }

    /// <summary>
    /// 상호작용 가능했던 객체를 제거합니다.
    /// </summary>
    /// <param name="npc"></param>
    public void RemoveInteractableNpc(NpcBase npc)
    {
        // 추가되어있지 않은 NPC 인 경우 재외
        if (!_InteractableNpcs.Contains(npc)) return;

        npc.OnInteractableFinished();

        _InteractableNpcs.Remove(npc);

        // 가까운 순으로 정렬
        _InteractableNpcs.Sort((a, b) =>
        Vector3.Distance(a.transform.position, transform.position) <
        Vector3.Distance(b.transform.position, transform.position) ? -1 : 1);
    }

    /// <summary>
    /// 상호작용을 시도합니다.
    /// </summary>
    public void TryInteraction()
    {
        if(_InteractableNpcs.Count == 0) return;
        _InteractableNpcs[0].OnInteraction();
    }
}
