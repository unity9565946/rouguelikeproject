using startup.single;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerCharacterMovement : MonoBehaviour
{
    [Header("# 이동 관련")]
    /// <summary>
    /// 걷기 속력
    /// </summary>
    public float m_WalkSpeed = 3.0f;

    /// <summary>
    /// 달리기 속력
    /// </summary>
    public float m_RunSpeed = 6.0f;

    /// <summary>
    /// 가속력
    /// </summary>
    public float m_AccelerationForce;

    /// <summary>
    /// 제동력
    /// </summary>
    public float m_BrakingForce;

    /// <summary>
    /// Yaw 회전 속력
    /// </summary>
    public float m_YawRotationSpeed = 360.0f;

    [Header("# 점프 관련")]
    /// <summary>
    /// 점프 힘
    /// </summary>
    public float m_JumpPower = 20.0f;

    /// <summary>
    /// 중력에 곱해질 승수
    /// </summary>
    public float m_GravityMultiplier = 0.02f;

    /// <summary>
    /// 떨어지는 최대 속력
    /// </summary>
    public float m_MaxFalloffSpeed = -0.5f;

    /// <summary>
    /// 지형 충돌 제외 레이어
    /// </summary>
    public LayerMask m_ExclusionLayer;

    /// <summary>
    /// 캐릭터 컨트롤러 컴포넌트입니다.
    /// </summary>
    private CharacterController _CharacterController;

    /// <summary>
    /// 입력 키를 벡터로 나타냅니다.
    /// x = R / L
    /// y = F / B
    /// </summary>
    private Vector2 _InputVector;

    /// <summary>
    /// 현재 적용된 속도입니다.
    /// </summary>
    private Vector3 _CurrentVelocity;

    /// <summary>
    /// 목표 속도입니다.
    /// </summary>
    private Vector3 _TargetVelocity;

    /// <summary>
    /// 캐릭터 이동 방향을 결정할 뷰 트랜스폼입니다.
    /// </summary>
    private Transform _ViewTransform;

    /// <summary>
    /// 목표 Yaw 회전값입니다.
    /// </summary>
    private float _TargetYawAngle;

    /// <summary>
    /// 땅에 닿아있음을 나타냅니다.
    /// </summary>
    private bool _IsGrounded;

    /// <summary>
    /// 점프 입력이 들어왔음을 나타냅니다.
    /// </summary>
    private bool _IsJump;

    /// <summary>
    /// 달리기 상태를 나타냅니다.
    /// </summary>
    private bool _IsRun;

    /// <summary>
    /// 이동 입력 허용 여부를 나타냅니다.
    /// </summary>
    private bool _AllowMovementInput = true;

    /// <summary>
    /// 충격 속도입니다.
    /// </summary>
    private Vector3 _ImpulseVelocity;

    public CharacterController characterController => _CharacterController ??
        (_CharacterController = GetComponent<CharacterController>());

    /// <summary>
    /// 속도에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    public Vector3 velocity => _CurrentVelocity;

    /// <summary>
    /// 땅에 닿음 상태에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    public bool isGrounded => _IsGrounded;

    #region DEBUG
    private DrawGizmoSphereInfo _GizmoSphereCheckFloorInfo;
    #endregion


    private void Awake()
    {
        _ViewTransform = SceneManagerBase.instance.
            GetSceneInstance<GameSceneInstance>().m_UseCamera.transform;

        _CharacterController = GetComponent<CharacterController>();
    }

    private void FixedUpdate()
    {
        // 가속력 / 제동력 계산
        CalculateAccelerationOrBrakingForce();

        // 바닥에 닿아있음을 확인합니다.
        _IsGrounded = IsGrounded();

        // 중력을 계산합니다.
        CalculateGravity(_IsGrounded);

        // 이동 방향으로 회전합니다.
        OrientRotation();

        // 충격 속도를 연산합니다.
        _CurrentVelocity += _ImpulseVelocity;
        _ImpulseVelocity = Vector3.Lerp(_ImpulseVelocity, Vector3.zero, m_BrakingForce);

        // 이동
        _CharacterController.Move(_CurrentVelocity * Time.fixedDeltaTime);
    }

    private void Update()
    {
        // 입력을 목표 속도로 변환합니다.
        InputToTargetVelocity();
    }


    /// <summary>
    /// 이동 방향으로 캐릭터를 회전시킵니다.
    /// </summary>
    private void OrientRotation()
    {
        // 이동 입력이 없다면 회전 X
        if (_InputVector.sqrMagnitude < Mathf.Epsilon) return;

        // 뷰 방향의 각도를 구합니다.
        Vector3 viewForward = _ViewTransform.forward;

        // 뷰 방향의 각도를 구합니다.
        float viewForwardAngle = Mathf.Atan2(viewForward.x, viewForward.z) * Mathf.Rad2Deg;

        // 입력한 축의 각도를 구합니다.
        // (0, 1) 을 입력한 경우 0' 가 나올 수 있도록 합니다.
        float inputAngle = Mathf.Atan2(_InputVector.x, _InputVector.y) * Mathf.Rad2Deg;

        // 목표 회전값을 계산합니다.
        // -> 뷰 방향(앞 방향) 각도 + 입력 방향 각도
        _TargetYawAngle = viewForwardAngle + inputAngle;

        // 자연스럽게 회전시키기 위한 다음 회전 각을 계산합니다.
        float newAngle = Mathf.MoveTowardsAngle(
            transform.eulerAngles.y,
            _TargetYawAngle,
            m_YawRotationSpeed * Time.fixedDeltaTime);

        // 회전시킵니다.
        transform.rotation = Quaternion.Euler(0f, newAngle, 0f);
    }

    private void CalculateGravity(bool isOnTheFloor)
    {
        // 중력을 계산합니다.
        float engineGravity = Physics.gravity.y * m_GravityMultiplier * Time.fixedDeltaTime;

        // 땅에 닿아있는 경우
        if (isOnTheFloor)
        {
            if (_IsJump)
            {
                _CurrentVelocity.y = m_JumpPower;
                _IsJump = false;
            }
        }
        // 땅에 닿아있지 않은 경우
        else
        {
            // 중력을 적용합니다.
            _CurrentVelocity.y += engineGravity;
        }
    }

    /// <summary>
    /// 바닥에 닿아있는지 확인합니다.
    /// </summary>
    /// <returns></returns>
    private bool IsGrounded()
    {
        // 바닥 검사를 위해 실행되는 Spherecast 길이
        float checkLength = _CharacterController.height;

        // 아래를 향해 SphereCast 를 진행합니다.
        RaycastHit hitResult;
        bool isHit = CheckCollision(
            out _GizmoSphereCheckFloorInfo, 
            transform.position, 
            Vector3.down, 
            checkLength, 
            out hitResult);

        // 바닥을 감지한 경우
        if (isHit)
        {
            // 캐릭터 캡슐의 높이 절반을 계산합니다.
            float halfHeight = _CharacterController.height * 0.5f;

            // 감지된 바닥의 Y 위치를 얻습니다.
            float detectedFloorYPos = hitResult.point.y;

            // 캐릭터 발 위치
            float characterBottomYPos = transform.position.y - halfHeight - _CharacterController.skinWidth;

            // 캐릭터 발 위치가 바닥 위치보다 낮을 경우 바닥에 닿아있음.
            return (detectedFloorYPos >= characterBottomYPos - 0.001f);
        }

        return false;
    }

    private bool CheckCollision(
        out DrawGizmoSphereInfo drawGizmoSphereInfo,
        Vector3 start, 
        Vector3 direction, 
        float maxDistance, 
        out RaycastHit hitResult)
    {
        Ray ray = new Ray(start, direction);
        float radius = _CharacterController.radius;

        int inclusiveLayer = ~m_ExclusionLayer;

        return PhysicsExt.SphereCast(
            out drawGizmoSphereInfo,
            ray, radius, out hitResult, maxDistance, inclusiveLayer,
            QueryTriggerInteraction.Collide);
    }


    /// <summary>
    /// 입력을 목표 속도로 변환합니다.
    /// </summary>
    private void InputToTargetVelocity()
    {
        // 입력 벡터를 뷰 방향으로 전환합니다.
        Vector3 inputToViewDirection = ConvertInputDirectionToViewDirection(_InputVector, _ViewTransform);

        // 목표 속도를 설정합니다.
        _TargetVelocity = inputToViewDirection * (_IsRun ? m_RunSpeed : m_WalkSpeed);
    }

    /// <summary>
    /// 가속력 / 제동력을 계산합니다.
    /// </summary>
    private void CalculateAccelerationOrBrakingForce()
    {
        // 제동중임을 나타냅니다.
        bool isBraking = _CurrentVelocity.sqrMagnitude > _TargetVelocity.sqrMagnitude;

        // CharacterController 에 현재 적용된 속도를 얻습니다.
        Vector3 currentVelocity = _CharacterController.velocity;
        float yVelocity = currentVelocity.y;
        currentVelocity.y = 0.0f;

        // 현재 속도를 목표 속도로 변경합니다.
        _CurrentVelocity = Vector3.MoveTowards(
            currentVelocity,
            _TargetVelocity,
            isBraking ? m_BrakingForce : m_AccelerationForce);
        // Vector3.MoveTowards(current, target, maxDistanceDelta) 
        // 지정한 속도(maxDistanceDelta) 만큼 current 에서 target 으로 변경합니다.

        // Y 속도를 적용시킵니다.
        _CurrentVelocity.y = yVelocity;
    }

    /// <summary>
    /// 입력 방향을 뷰 방향으로 변환합니다.
    /// </summary>
    /// <param name="inputDirection">변환시킬 입력 방향을 전달합니다.</param>
    /// <param name="viewTransform">변환의 기준이 될 뷰 트랜스폼을 전달합니다.</param>
    /// <returns></returns>
    private Vector3 ConvertInputDirectionToViewDirection(
        Vector2 inputDirection, 
        Transform viewTransform)
    {
        Vector3 viewForward = viewTransform.forward;
        viewForward.y = 0.0f;
        viewForward.Normalize();
        Vector3 viewRight = viewTransform.right;

        viewForward *= inputDirection.y;
        viewRight *= inputDirection.x;


        return (viewForward + viewRight).normalized;
    }


    public void OnMovementInput(Vector2 axisValue)
    {
        _InputVector = _AllowMovementInput ? axisValue : Vector2.zero;
    }

    public void OnJumpInput()
    {
        if (!_AllowMovementInput) return;

        if (!_IsJump && _IsGrounded)
        {
            _IsJump = true;
        }
    }

    public void RunStart()
    {
        _IsRun = true;
    }

    public void RunFinish()
    {
        _IsRun = false;
    }

    /// <summary>
    /// 이동 입력을 블록시킵니다.
    /// </summary>
    public void BlockMovementInput()
    {
        _AllowMovementInput = false;
        _InputVector = Vector3.zero;
        _TargetVelocity = Vector3.zero;
    }

    /// <summary>
    /// 이동 입력을 허용합니다.
    /// </summary>
    public void AllowMovementInput()
    {
        _AllowMovementInput = true;
    }

    /// <summary>
    /// 충격이동을 추가합니다.
    /// </summary>
    /// <param name="direction">충격 방향</param>
    /// <param name="power">힘</param>
    public void AddImpulse(Vector3 direction, float power)
    {
        _ImpulseVelocity += direction * power;
    }


#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        PhysicsExt.DrawGizmoSphere(in _GizmoSphereCheckFloorInfo);
    }
#endif

}

