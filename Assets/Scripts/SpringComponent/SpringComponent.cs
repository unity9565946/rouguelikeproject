using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringComponent : MonoBehaviour
{
    [Header("# 스프링 거리")]
    public float m_SpringZoomMultiplier = 0.3f;
    public float m_SpringDistanceMin = 1.0f;
    public float m_SpringDistanceMax = 3.0f;

    [Header("# 충돌체 감지 관련")]
    /// <summary>
    /// 충돌체 감지에서 제외시킬 레이어
    /// </summary>
    public LayerMask m_ExclusionLayer;

    [Header("# 회전")]

    /// <summary>
    /// 회전 속도에 곱해질 승수
    /// </summary>
    public float m_TurnSpeedMultiplier = 1.0f;

    /// <summary>
    /// 최대 Pitch 회전값을 나타냅니다.
    /// </summary>
    public float _PitchAngleLimitMax;

    /// <summary>
    /// 최소 Pitch 회전값을 나타냅니다.
    /// </summary>
    public float _PitchAngleLimitMin;

    /// <summary>
    /// 스프링 이동시킬 트랜스폼을 나타냅니다.
    /// </summary>
    private Transform _SpringTargetTransform;

    /// <summary>
    /// 적용된 Yaw 회전값을 나타냅니다.
    /// </summary>
    private float _YawAngle;

    /// <summary>
    /// 적용된 Pitch 회전값을 나타냅니다.
    /// </summary>
    private float _PitchAngle;

    /// <summary>
    /// 다른 뷰를 사용하고 있음을 나타냅니다.
    /// </summary>
    private bool _UseOtherView;

    /// <summary>
    /// 뷰 타깃 위치입니다.
    /// </summary>
    private Vector3 _ViewTargetPosition;

    /// <summary>
    /// 뷰 타깃 회전입니다.
    /// </summary>
    private Quaternion _ViewTargetRotation;

    /// <summary>
    /// 사용자가 설정한 거리이며, 실제 적용될 거리입니다.
    /// </summary>
    public float currentLength { get; set; }

    #region DEBUG
    private DrawGizmoLineInfo _DrawGizmoLineInfo;
    #endregion



    private void Awake()
    {
        // 초기 길이를 설정합니다.
        currentLength = m_SpringDistanceMin + ((m_SpringDistanceMax - m_SpringDistanceMin) * 0.5f);
    }

    private void Update()
    {
        if(_UseOtherView)
        {
            Vector3 currentPosition = _SpringTargetTransform.transform.position;
            currentPosition = Vector3.Lerp(currentPosition, _ViewTargetPosition, 0.2f);

            Quaternion currentRotation = _SpringTargetTransform.transform.rotation;
            currentRotation = Quaternion.Lerp(currentRotation, _ViewTargetRotation, 0.2f);

            _SpringTargetTransform.position = currentPosition;
            _SpringTargetTransform.rotation = currentRotation;
        }
        else
        {
            // 충돌체 확인
            float length = DoCollisionTest();

            // 길이를 갱신합니다.
            UpdateSpringLength(length);

            // 회전 갱신
            UpdateRotation();

        }
    }

    /// <summary>
    /// 회전을 갱신합니다.
    /// </summary>
    private void UpdateRotation()
    {
        transform.rotation = Quaternion.Euler(_PitchAngle, _YawAngle, 0.0f);
    }


    /// <summary>
    /// 충돌체 검사를 진행합니다.
    /// </summary>
    /// <returns>충돌체가 감지된다면 충돌체까지의 거리를 반환하고, 그렇지 않다면 사용자가 설정한 거리를 반합니다.</returns>
    private float DoCollisionTest()
    {
        // 스프링 컴포넌틍 위치를 레이 시작점으로 설정합니다.
        Vector3 rayStart = transform.position;

        // 레이를 발사시킬 방향
        Vector3 rayDirection = transform.forward * -1;

        // 레이 구조체를 완성합니다.
        Ray ray = new Ray(rayStart, rayDirection);

        // 충돌 검사에 포함시킬 레이어
        int inclusiveLayer = ~m_ExclusionLayer;

        // 레이 캐스트를 진행합니다.
        RaycastHit hitResult;
        bool isHit = PhysicsExt.RayCast(
            out _DrawGizmoLineInfo,
            ray, out hitResult, currentLength, inclusiveLayer,
            QueryTriggerInteraction.Ignore);

        // 감지된 객체가 존재하는 경우 감지 거리를 반환하고,
        // 아니라면 사용자가 설정한 거리를 반환합니다.
        return (isHit ? hitResult.distance : currentLength);
    }



    /// <summary>
    /// 스프링 길이를 갱신합니다.
    /// </summary>
    /// <param name="length"></param>
    private void UpdateSpringLength(float length)
    {
        _SpringTargetTransform.localPosition = Vector3.back * length;
    }


    /// <summary>
    /// 스프링 이동시킬 트랜스폼을 설정합니다.
    /// </summary>
    /// <param name="target"></param>
    public void SetTargetTransform(Transform target)
    {
        _SpringTargetTransform = target;
        _SpringTargetTransform.SetParent(transform);

        _SpringTargetTransform.localPosition = Vector3.zero;
        _SpringTargetTransform.rotation = Quaternion.identity;
    }


    public void Turn(float addYawAngle, float addPitchAngle)
    {
        _YawAngle += addYawAngle * m_TurnSpeedMultiplier;
        _PitchAngle -= addPitchAngle * m_TurnSpeedMultiplier;
        _PitchAngle = Mathf.Clamp(_PitchAngle, _PitchAngleLimitMin, _PitchAngleLimitMax);
    }

    public void AddLength(float addAxis)
    {
        currentLength -= addAxis * m_SpringZoomMultiplier;
        currentLength = Mathf.Clamp(currentLength, m_SpringDistanceMin, m_SpringDistanceMax);
    }

    public void SetViewTarget(Transform viewTargetTransform)
    {
        _UseOtherView = true;
        _ViewTargetPosition = viewTargetTransform.position;
        _ViewTargetRotation = viewTargetTransform.rotation;
    }

    public void CancelViewTarget()
    {
        _UseOtherView = false;
        
        // 위치, 회전 초기화
        _SpringTargetTransform.localPosition = Vector3.zero;

        _SpringTargetTransform.localRotation = Quaternion.identity;

    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        PhysicsExt.DrawGizmoLine(in _DrawGizmoLineInfo);
    }
#endif
}
