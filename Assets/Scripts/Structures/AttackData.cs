using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class AttackDataElem
{
    public string m_AttackName;
    public AttackData m_AttackData;
}


[System.Serializable]
public struct AttackData
{
    /// <summary>
    /// 최대 콤보 카운트
    /// </summary>
    public int maxComboCount;

    /// <summary>
    /// 콤보별 재생시킬 애니메이션 이름
    /// </summary>
    public List<string> animationNames;

    /// <summary>
    /// 콤보별 적용시킬 대미지
    /// </summary>
    public List<float> attackDamages;
}


