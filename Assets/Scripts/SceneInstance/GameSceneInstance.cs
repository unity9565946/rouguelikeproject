using startup.sceneInstance;
using System.Collections.Generic;
using UnityEngine;

public sealed class GameSceneInstance : DefaultSceneInstance
{
    [Header("# 플레이어 시작 위치 관련")]
    /// <summary>
    /// 사용할 플레이어 시작 위치 인덱스를 나타냅니다.
    /// </summary>
    public int m_UsePlayerStartIndex;

    /// <summary>
    /// 플레이어 시작 위치를 나타내는 객체들입니다.
    /// 기본적으로 m_UsePlayerStartIndex 번째 요소를 사용합니다.
    /// </summary>
    public List<PlayerStart> m_PlayerStart;


    [Header("# 사용될 플레이어 컨트롤러 프리팹")]
    public PlayerController m_PlayerControllerPrefab;

    [Header("# 사용될 플레이어 캐릭터 프리팹")]
    public PlayerCharacter m_PlayerCharacter;

    [Header("# 사용될 카메라 컴포넌트")]
    public Camera m_UseCamera;

    [Header("# 사용될 Player UI 객체")]
    public PlayerUI m_UsePlayerUI;


    private AttackResultHUDController _AttackResultHUDController;

    /// <summary>
    /// 사용되는 적 캐릭터 객체들을 나타냅니다.
    /// </summary>
    private Dictionary<Collider, EnemyCharacter> _UseEnemyCharacters = new();

    public AttackResultHUDController attackResultHUDController => _AttackResultHUDController ?? (_AttackResultHUDController = GetComponent<AttackResultHUDController>());


    /// <summary>
    /// 사용하고 있는 플레이어 컨트롤러 객체에 대한 프로퍼티입니다.
    /// </summary>
    public PlayerController playerController { get; private set; }

    private void Awake()
    {
        // 플레이어 시작 위치, 회전을 얻습니다,.
        bool bFound = GetPlayerStartPositionAndYawRotation(
            out Vector3 startPosition, 
            out float yawAngle);

#if UNITY_EDITOR
        if (!bFound)
        {
            Debug.LogWarning("시작 위치를 찾을 수 없습니다.");
        }
#endif

        // 플레이어 컨트롤러를 생성합니다.
        playerController = CreatePlayerController();

        // 플레이어 캐릭터를 생성합니다.
        PlayerCharacter newPlayerCharacter = CreatePlayerCharacter(startPosition, yawAngle);

        // 플레이어 캐릭터 조종 시작
        playerController.StartControl(newPlayerCharacter);
    }

    /// <summary>
    /// 플레이어 컨트롤러를 생성합니다.
    /// </summary>
    /// <returns></returns>
    private PlayerController CreatePlayerController()
    {
        return Instantiate(m_PlayerControllerPrefab);
    }

    /// <summary>
    /// 플레이어 캐릭터를 생성합니다.
    /// </summary>
    /// <param name="position">생성될 위치를 전달합니다.</param>
    /// <param name="yawRotation">적용될 회전을 전달합니다.</param>
    /// <returns></returns>
    private PlayerCharacter CreatePlayerCharacter(Vector3 position, float yawRotation)
    {
        PlayerCharacter newCharacter = Instantiate(m_PlayerCharacter, 
            position, 
            Quaternion.Euler(Vector3.up * yawRotation));

        return newCharacter;
    }

    private bool GetPlayerStartPositionAndYawRotation(out Vector3 position, out float yawAngle)
    {
        // 시작 위치가 등록되어있지 않거나, 잘못된 시작 위치 인덱스를 사용한다면 기본 값을 반환합니다.
        if (m_PlayerStart.Count == 0 || m_PlayerStart.Count <= m_UsePlayerStartIndex)
        {
            position = Vector3.zero;
            yawAngle = 0.0f;
            return false;
        }

        // 시작 위치, 회전을 얻습니다.
        PlayerStart playerStart = m_PlayerStart[m_UsePlayerStartIndex];
        position = playerStart.startPosition;
        yawAngle = playerStart.transform.eulerAngles.y;

        return true;
    }

    /// <summary>
    /// 적 객체를 등록합니다.
    /// </summary>
    /// <param name="newEnemyCharacter">등록시킬 적 객체를 전달합니다.</param>
    public void RegisterEnemyCharacter(EnemyCharacter newEnemyCharacter)
    {
        _UseEnemyCharacters.Add(newEnemyCharacter.enemyCollider, newEnemyCharacter);
    }

    /// <summary>
    /// 컬라이더를 통해 등록된 적 객체를 얻습니다.
    /// </summary>
    /// <param name="enemyCollider"></param>
    /// <returns></returns>
    public EnemyCharacter GetEnemyCharacterFromCollider(Collider enemyCollider)
    {
        if (_UseEnemyCharacters.TryGetValue(enemyCollider, out EnemyCharacter enemyCharacter))
        {
            return enemyCharacter;
        }
        else return null;
    }

}
