using startup.single;
using System.Collections.Generic;
using UnityEngine;

public class AttackResultHUDController : MonoBehaviour
{
    [Header("# 공격 결과HUD 프리팹")]
    public AttackResultHUD m_AttackResultHUDPrefab;

    private List<AttackResultHUD> _AttackResultHUDs = new();

    public void ShowResult(Vector3 hitPosition)
    {
        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        PlayerUI playerUI = sceneInstance.m_UsePlayerUI;

        AttackResultHUD attackResultHUD = playerUI.CreateHUD(m_AttackResultHUDPrefab);

        attackResultHUD.SetWorldPosition(hitPosition);
        attackResultHUD.InitializeAttackResultHUD(this);

        _AttackResultHUDs.Add(attackResultHUD);
    }

    public void DestroyAttackResultHUD(AttackResultHUD attackResultHUDInstance)
    {
        _AttackResultHUDs.Remove(attackResultHUDInstance);
        Destroy(attackResultHUDInstance.gameObject);
    }

}